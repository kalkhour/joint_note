The results of the characterization of the HLC1 and LAUROC0 chips as detailed in the
previous sections, show that these ASICs are good prototypes for a first generation of
test chips, in that they already meet some of the important requirements expected from the
analogue front-end preamplifiers/shapers. But a few significant points need special
attention for the design of the second generation of test chips.

First, these prototypes have demonstrated the possibility to build a single ASIC that can
be adapted to all calorimeter cells, i.e function as a 10\,mA 25$\Omega$ or as a 2\,mA
50$\Omega$ preamplifier. In LAUROC0, the dedicated 25\_50 channel has performed as well as
the dedicated 25$\Omega$ and 50$\Omega$ channels in all measurements.

\paragraph{Input impedance tuning.} Both ASICs can be successfully tuned to match the
input impedance. The tuning range chosen by HLC1 is however a bit marginal, and more bits
are necessary to make it tuneable over a larger range so it can accomodate process
variations.

\paragraph{Linearity.} Both ASICs exhibit an excellent linearity with an INL<0.2\% up to
$\sim9\,$mA in the 25$\Omega$ configuration, and with a high gain non-linearity of 0.1\% up to
saturation. In the 50$\Omega$, the INL of the Low Gain of HLC1 is slightly worse, of the
order of 0.3\%. LAUROC0 exhibits a gain too large, and therefore saturates around 1.4\,mA.
Up to that point, its linearity is on par with that of the 25$\Omega$ configuration.

Another point of attention for LAUROC0 is its design with a switch on the High Gain output
based on a discriminator. This switch creates too large a local non-linearity on the Low
Gain output around the energies where the High Gain saturates.

\paragraph{Noise.} In order to compare the noise levels of the two ASICs, care has to be
taken to plot them as a function of the peaking time: the dominant series noise indeed
has a $\tau_p^{-3/2}$ dependence. Figure~\ref{fig:ENIComp} shows such a comparison for the
10\,mA, 25$\Omega$ and 2\,mA, 50$\Omega$ configurations. The
HLC1 measurements are those done at the output of the shapers using the ADCs on the FETB,
when varying the builtin peaking time tuning. The LAUROC0 measurements are performed with
an external variable shaper. The comparison is therefore a bit unfair as the noise induced
by the shaper stage is included in the HLC1 curve but not in the LAUROC0 curve.
Typical performance of the 0T preamplifiers is also shown for some reference calorimeter
cells, which have been rescaled to the same detector capacitance.

The noise levels achieved for the 10\,mA, 25$\Omega$ configuration
(Figure~\ref{fig:ENIComp} left) are very similar
between the two chips. They are already slightly below the specification for Phase-2,
which has been set so MIP deposits from muons are still measurable in the LAr, in order to
perform precise front-to-middle layer intercalibration. However, the noise achieved by the
0T preamplifiers is significantly lower in the 25$\Omega$ configuration. As already
discussed in the previous sections, a fraction of the noise in the chips is coming from a
design issue triggered by an incorrect simulation setup, and will be improved in the next
iteration.

The shaper settings of HLC1 lead to too slow pulses compared to what is required (see the
typical 0T points). Another aspect of the shaper that needs improvement is that the
peaking time of the Low Gain and High Gain outputs differ by 4--5\,ns, see
Table~\ref{tab:hlc1_pkt}. As LAUROC0 contains only the preamplifier, studies of the
properties of its shaper will be done for its next iteration.

For the 50$\Omega$, 2\,mA configuration, Figure~\ref{fig:ENIComp} (right) shows a much lower noise
for LAUROC0 compared to HLC1, and very close to the 0T performance. However this difference
comes at least partically from the fact that LAUROC0 dynamic range is roughly 1.4\,mA
rather than 2\,mA.

\begin{figure}
\begin{subfloat}
  \centering
  \includegraphics[width=.5\textwidth]{ENI_Comparison_25_pkt10_100.png}
  \label{fig:ENI25}
\end{subfloat}%
\begin{subfloat}
  \centering
  \includegraphics[width=.5\textwidth]{ENI_Comparison_50_pkt10_100.png}
  \label{fig:ENI50}
\end{subfloat}
\caption{Comparison of the noise levels of LAUROC0 and HLC1 as a function of peaking time
(10\%-100\%), in the 10\,mA 25$\Omega$ configuration for a 1.5\,nF cell (left) and 2\,mA 50$\Omega$
configuration for a 330\,pF cell (right). The HLC1 measurements are those done at the output of the shaper,
using the ADCs on the FETB. The LAUROC0 measurements are those done with an external
variable shaper. Reference points for the Phase 2 specifications and for typical
performance of the 0T preamplifiers are also plotted.}
\label{fig:ENIComp}
\end{figure}

\paragraph{Cross-talk.} The typical figure of merit that needs to be achieved by the
preamplifier/shaper chip is around 0.5\% peak-to-peak cross-talk. LAUROC0 reaches that
goal, although it is only at the preamplifier output so that needs to be demontrated when a
shaper stage is added in the chip. HLC1 achieves exactly 0.5\% for its 25$\Omega$
configuration, but the 50$\Omega$ configuration exhibits 2.5\% of cross-talk between
neighbouring channels, so that needs to be improved.

\paragraph{Other noteworthy points.} 
\begin{itemize}
  \item The first attempt at a trigger summing output in HLC1 presents saturation at
    relatively low energies. That needs to be revisited in next iterations
  \item The behaviour of the ASIC for complete LHC orbits in cases of very high pile-up
    levels has been investigated only for LAUROC0. The precise consequences of this
    behaviour and possible measures to circumvent it have to be understood for the next
    iterations.
\end{itemize}


